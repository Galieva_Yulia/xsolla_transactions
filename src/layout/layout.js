import { Component } from '../component';

import style from './layout.css';
import template from './layout.html';
import {getTemplate} from '../service.js'

export class Layout extends Component {
    constructor() {
        super();
       // this.attachTemplate(template, style);
        let tmpl = getTemplate(template, style);
        this.attachTemplate(tmpl);
    }
}