import { Component } from '../component';

import style from './transaction.css';
import template from './transaction.html';
import {getTemplate} from '../service.js'
export class Transaction extends Component {
    get registerDate() {
        return this._registerDate;
    }

    set registerDate(date) {
        this._registerDate = date;
        this.shadowRoot.getElementById('date').innerText = this._registerDate.toLocaleString();
    }

    constructor() {
        super();
        // this.attachTemplate(template, style);
        let tmpl = getTemplate(template, style);
        this.attachTemplate(tmpl);
        this.bindPropertiesToElements([
            'amount',
            'comment',
            'date',
            'sum',
            'transaction_type'
        ]);
        this.bindPropertiesToAttributes([
            'enabled'
        ]);
    }
}