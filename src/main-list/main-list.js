import { Component } from '../component';

import style from './main-list.css';
import template from './main-list.html';
import {getTemplate} from '../service.js'

export class MainList extends Component {

    constructor() {
        super();
        // this.attachTemplate(template, style);
        let tmpl = getTemplate(template, style);
        this.attachTemplate(tmpl);
        this.addShadowEventListener('ta-user-list', 'select', this.selectUser);
    }

    connectedCallback() {
        this.renderList();
    }

    selectUser(event) {
        this.shadowRoot.querySelector('ta-transaction-list').userId = event.detail.user_id;
    }
}