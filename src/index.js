import { Layout } from './layout/layout';
import { Pagination } from './pagination/pagination';
import { User } from './user/user';
import { UserList } from './user-list/user-list';

import { Transaction } from './transaction/transaction';
import { TransactionList } from './transaction-list/transaction-list';

import {MainList } from './main-list/main-list';


import { UserService } from './user-service';

import './index.css';

const userService = new UserService('https://livedemo.xsolla.com/fe/test-task/baev/users');

customElements.define('ta-layout', Layout);

customElements.define('ta-pagination', Pagination);

customElements.define('ta-main-list', MainList);


customElements.define('ta-user', User);
customElements.define('ta-transaction', Transaction);

UserList.userService = userService;
customElements.define('ta-user-list', UserList);
customElements.define('ta-transaction-list', TransactionList);

TransactionList.userService = userService;