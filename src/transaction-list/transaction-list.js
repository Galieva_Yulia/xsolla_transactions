import { Component } from '../component';

import style from './transaction-list.css';
import template from './transaction-list.html';
import {getTemplate} from '../service.js'
let dependencies = {};

export class TransactionList extends Component {
    static get userService() {
        return dependencies.userService;
    }

    static set userService(dependency) {
        dependencies.userService = dependency;
    }

    get transactions() {
        return Array.from(this.shadowRoot.querySelectorAll('ta-transaction'));
    }

    get userId() {
        return this.getAttribute('user-id');
    }

    set userId(value) {
        value ? this.setAttribute('user-id', value) : this.removeAttribute('user-id');
    }

    get list() {
        return this.shadowRoot.getElementById('list');
    }

    static get observedAttributes() {
        return ['user-id'];
    }

    constructor() {
        super();
        // this.attachTemplate(template, style);
        let tmpl = getTemplate(template, style);
        this.attachTemplate(tmpl);
        this.records = 20;
    }

    connectedCallback() {
        this.renderList();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'user-id')
            this.renderList();
    }


    emptyList() {
        while (this.list.hasChildNodes()) {
            this.list.removeChild(this.list.lastChild);
        }
    }


    renderList() {
        this.emptyList();
        TransactionList.userService.getTransactions(this.userId).then(response => {



          /*  if (response.data === undefined || response.data === null) {
                console.log('response', response)

                return;
            }*/
            response.forEach(transactionData => {
                console.log('tut',transactionData)

                const Transaction = customElements.get('ta-transaction'); //возвращает конструктор
                const transaction = new Transaction();

                transaction.amount = transactionData['amount'];
                transaction.comment = transactionData['comment'];
                transaction.sum = transactionData['sum'];
                transaction.date = new Date(transactionData['date']).toLocaleString();
                transaction.transaction_type = transactionData['transaction_type'];
                this.list.appendChild(transaction);
            });
        });
    }
}