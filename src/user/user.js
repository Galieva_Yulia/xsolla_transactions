import { Component } from '../component';

import style from './user.css';
import template from './user.html';
import {getTemplate} from '../service.js'
export class User extends Component {
    get registerDate() {
        return this._registerDate;
    }

    set registerDate(date) {
        this._registerDate = date;
        this.shadowRoot.getElementById('register-date').innerText = this._registerDate.toLocaleString();
    }

    constructor() {
        super();
        // this.attachTemplate(template, style);
        let tmpl = getTemplate(template, style);
        this.attachTemplate(tmpl);

        this.bindPropertiesToElements([
            'name',
            'custom',
            'balance',
            'email',
            'walletAmount',
            'walletCurrency'
        ]);
        this.bindPropertiesToAttributes([
            'enabled'
        ]);
    }
}