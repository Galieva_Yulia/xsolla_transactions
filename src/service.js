export let getTemplate = (template, style) => {
    style = style ? '<style>' + style + '</style>' : '';
    let tmpl = document.createElement('template');
    tmpl.innerHTML = style + template;

    return tmpl;
};

